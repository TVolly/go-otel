generate:
	rm -rf ./protogen && mkdir ./protogen
	protoc --go_out=./protogen --go-grpc_out=./protogen ./proto/*.proto
