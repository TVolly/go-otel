FROM golang:1.22-alpine as builder

WORKDIR /app

RUN apk update && apk add openrc openssh git

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .

RUN go build -o /app/app ./cmd/app

FROM alpine

WORKDIR /app

COPY --from=builder /app/app ./app

EXPOSE 8080

CMD ["./app"]
