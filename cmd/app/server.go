package main

import (
	"context"

	desc "example.com/gootel/protogen/demo"
	"google.golang.org/protobuf/types/known/emptypb"
)

type server struct {
	desc.UnimplementedDemoServiceServer
}

func (s *server) MethodA(ctx context.Context, in *emptypb.Empty) (*emptypb.Empty, error) {
	return &emptypb.Empty{}, nil
}
