package main

import (
	"context"
	"errors"
	"io"
	"log/slog"
	"net/http"
	"os"

	"github.com/aws/aws-xray-sdk-go/xray"
	"github.com/aws/aws-xray-sdk-go/xraylog"
)

func init() {
	slog.Info("init xray\n")

	xray.Configure(xray.Config{
		// DaemonAddr:     "127.0.0.1:2000",
		ServiceVersion: "1.2.3",
	})
	xray.SetLogger(xraylog.NewDefaultLogger(os.Stdout, xraylog.LogLevelDebug))
}

func getRoot(w http.ResponseWriter, r *http.Request) {
	ctx, seg := xray.BeginSegment(context.Background(), "service-name")
	// Start a subsegment
	_, subSeg := xray.BeginSubsegment(ctx, "subsegment-name")
	// ...
	slog.Info("got / request\n")
	io.WriteString(w, "This is my website!\n")
	// ...
	subSeg.Close(nil)
	// Close the segment
	seg.Close(nil)
}

func getHello(w http.ResponseWriter, r *http.Request) {
	slog.Info("got /hello request\n")
	io.WriteString(w, "Hello, HTTP!\n")
}

func main() {

	http.HandleFunc("/", getRoot)
	http.HandleFunc("/hello", getHello)

	slog.Info("listen and serve 8080\n")
	err := http.ListenAndServe(":8080", nil)
	if errors.Is(err, http.ErrServerClosed) {
		slog.Info("server closed\n")
	} else if err != nil {
		slog.Info("error starting server: %s\n", err)
		os.Exit(1)
	}

	// grpcServer := grpc.NewServer(
	// 	grpc.UnaryInterceptor(
	// 		xray.UnaryServerInterceptor(),
	// 	),
	// )

	// desc.RegisterDemoServiceServer(grpcServer, new(server))

	// lis, err := net.Listen("tcp", ":8080")
	// if err != nil {
	// 	slog.Error(err.Error())
	// 	return
	// }

	// slog.Info("serve server", slog.String("addr", ":8080"))
	// if err := grpcServer.Serve(lis); err != nil {
	// 	if !errors.Is(err, grpc.ErrServerStopped) {
	// 		slog.Error("stop failed")
	// 	}
	// }
}
